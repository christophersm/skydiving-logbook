# Skydiving Logbook

## Table of contents

- [General-Info](#general-info)
- [Prerequisites](#prerequisites)
- [Technologies-Used](#technologies-used)
- [Setup](#setup)
- [Testing-Git-Repository](#testing-git-repository)

## General-Info

This is a c# skydiving logbook application. This project will grow with me as I learn more aspects and code functionality through my educational journey.

## Prerequisites

- .NetCore Framework 3.1 needs installed

## Technologies-Used

Project files will be created with:

- C#

Projects will be saved in GitLab

- <xhttps://gitlab.com/christophersm/skydiving-logbook>

## Setup

To work with this project:

- Clone the repository to your desired location

## Testing-Git-Repository

### Steps

1. - Clone Repo to desired location
2. - Execute dotnet run on sln
3. - run git status
4. - Verify no untracked files are displayed
5. - Verify only a .csproj, .sln, and program.cs file are displayed

### Testing-Each-application

1. - Import project into Visual Studio or VS Code
2. - Build the Solution, this will generate the needed local files
3. - In console, browse to /Project location/Project Name/bin/Debug/netcoreapp3.1/
4. - type " .\Projectname.exe
5. - Output displayed

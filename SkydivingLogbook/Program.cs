﻿using System;
using System.Threading;
using System.IO;

namespace SkydivingLogbook
{
    class Program
    {
        //Setup an Intro Method that is called at the beginning of my main method
        static void Intro()
        {
            Console.WriteLine("Welcome to your skydiving logbook!");
            Thread.Sleep(3000);
            Console.WriteLine("Please enter the details about your jump.");
            Thread.Sleep(3000);
        }

        
        static string AskQuestion(string question)
        {
            Console.Write(question + " ");
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {

            //Call Intro Method
            Intro();

            // Start of logbook entries
            string membershipNumber = AskQuestion("Enter USPA Membership Number (ex. 00000000):");

            string firstName = AskQuestion("Enter your full first name:");

            string middleInitial = AskQuestion("Enter your Middle Initial:");
           
            string lastName = AskQuestion("Enter your last name:");
         
            string yourAge = AskQuestion("Enter your age:");
            
            string dateofBirth = AskQuestion("Enter your Date of Birth (ex. 00/00/0000):");
        
            string currentWeight = AskQuestion("Enter your current weight(in pounds):");
   
            string dateOfJump = AskQuestion("Enter the date of the jump (ex. 00/00/0000):");
           
            string timeOfJump = AskQuestion("Enter the time of the jump (ex. 0900 for 9am and 1300 for 1pm):");
                   
            string facilityName = AskQuestion("Enter the name of the facility:");
     
            string facilityAddress = AskQuestion("Enter the address of the facility:");
          
            string jumpAltitude = AskQuestion("Enter the jump altitude:");
               
            string groundWindSpeed = AskQuestion("Enter the ground speed, in knots, at the time of the jump:");
        
            string trainingAttempted = AskQuestion("Enter any training manuevers attempted (Enter NA if none were attempted):");
           
            string trainingSuccessful = AskQuestion("Enter if the training manuever was completed successfully (Enter NA if none were attempted):");
          
            // Write output to console logs. Allows users to see data entered.
            Console.WriteLine("USPA Membership Number: " + membershipNumber);
            Console.WriteLine("First name: " + firstName);
            Console.WriteLine("Middle Initial: " + middleInitial);
            Console.WriteLine("Last Name: " + lastName);
            Console.WriteLine("Your Age: " + yourAge);
            Console.WriteLine("Date of birth: " + dateofBirth);
            Console.WriteLine("Current weight: " + currentWeight + " pounds");
            Console.WriteLine("Date of jump: " + dateOfJump);
            Console.WriteLine("Time of jump: " + timeOfJump);
            Console.WriteLine("Skydiving facility: " + facilityName);
            Console.WriteLine("Facility address: " + facilityAddress);
            Console.WriteLine("Jump Altitude: " + jumpAltitude + " feet");
            Console.WriteLine("Ground wind speed: " + groundWindSpeed + " kts");
            Console.WriteLine("Training manuevers attempted: " + trainingAttempted);
            Console.WriteLine("Training manuevers were successful: " + trainingSuccessful);

            // Write output to txt file
            string path = @"SkyDivingLogFiles";
            if(!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            using (StreamWriter writer = new StreamWriter(Path.Combine(@path, "SkydivingLogFile_" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss") + ".txt")))
            {
                writer.WriteLine("USPA Membership Number: " + membershipNumber);
                writer.WriteLine("First name: " + firstName);
                writer.WriteLine("Middle Initial: " + middleInitial);
                writer.WriteLine("Last Name: " + lastName);
                writer.WriteLine("Your Age: " + yourAge);
                writer.WriteLine("Date of birth: " + dateofBirth);
                writer.WriteLine("Current weight: " + currentWeight + " pounds");
                writer.WriteLine("Date of jump: " + dateOfJump);
                writer.WriteLine("Time of jump: " + timeOfJump);
                writer.WriteLine("Skydiving facility: " + facilityName);
                writer.WriteLine("Facility address: " + facilityAddress);
                writer.WriteLine("Jump Altitude: " + jumpAltitude + " feet");
                writer.WriteLine("Ground wind speed: " + groundWindSpeed + " kts");
                writer.WriteLine("Training maneuvers attempted: " + trainingAttempted);
                writer.WriteLine("Training maneuvers were successful: " + trainingSuccessful);
            }
        }
    }
}